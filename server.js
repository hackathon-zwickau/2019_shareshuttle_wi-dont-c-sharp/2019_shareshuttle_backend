const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const urlBuilder = new(require('./classes/URLBuilder.js'));

MongoClient.connect('testuser:testpw@mongodb://167.172.161.43:27017', function (err, db) {
	if (err) {
		console.error(err.message);
	}

	let dbo = db.db("testing");

	dbo.createCollection("test", function (err, res) {
		if (err) throw err;
		console.log("Collection created!");
		db.close();
	});
});

const app = express();
const port = 8000;
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(bodyParser.json());

app.get('/', function (req, res) {
	let url = urlBuilder.build(52.215676, 5.963946, 52.2573, 6.1799);

	http.get(url, (resp) => {
		let data = '';
		// A chunk of data has been recieved.
		resp.on('data', (chunk) => {
			data += chunk;
		});

		// The whole response has been received. Print out the result.
		resp.on('end', () => {
			res.send(data);
		});

	}).on("error", (err) => {
		console.log("Error: " + err.message);
	});
});

app.post('/authenticate', (req, res) => {
	console.log(req.query.id);
	res.send("Success!");
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
