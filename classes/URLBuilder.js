const ApiBaseUrl = 'http://www.yournavigation.org/api/1.0/gosmore.php?format=geojson&v=motorcar&fast=1&layer=mapnik&';

module.exports = class URLBuilder {
	build(flat, flon, tlat, tlon, lang = "de") {

		return (ApiBaseUrl + `flat=${flat}&flon=${flon}&tlat=${tlat}&tlon=${tlon}&lang=${lang}`);
	}
}
